const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const config = require('./config/keys');

const mongoose = require('mongoose');
mongoose.connect(  'mongodb+srv://emel:Frontend00@otechbot-frki3.mongodb.net/chatbot?retryWrites=true&w=majority', { useNewUrlParser: true });


mongoose.connection.on('connected' ,()=>{
    console.log('connected ')
});


app.use(bodyParser.json());

require('./routes/dialogFlowRoutes')(app);

require('./routes/fulfillmentRoutes')(app);

const PORT = process.env.PORT || 5000;
app.listen(PORT);