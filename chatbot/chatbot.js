'use strict';
const dialogflow = require('dialogflow');
const structjsnon = require('./structjson');
const config = require('../config/keys');
const mongoose = require('mongoose');

const projectId = config.googleProjectID;
const sessionId = config.dialogFlowSessionID;
const languageCode = config.dialogFlowSessionLanguageCode;

const credentials = {
    client_email: config.googleClientEmail,
    private_key:
    config.googlePrivateKey
};

const sessionClient = new dialogflow.SessionsClient({
    projectId,
    credentials
});

const Registration = require('../models/Registration');
const Request = require('../models/Request');

module.exports = {
    textQuery: async function (text, userID, parameters = {}) {
        let self = module.exports;
        const sessionPath = sessionClient.sessionPath(projectId, sessionId + userID);

        const request = {
            session: sessionPath,
            queryInput: {
                text: {
                    text: text,
                    languageCode: languageCode
                }
            },
            queryParams: {
                payload: {
                    data: parameters
                }
            }
        };

        let responses = await sessionClient.detectIntent(request);
        responses = await self.handleAction(responses);
        return responses;
    },

    eventQuery: async function (event, userID, parameters = {}) {
        let self = module.exports;
        let sessionPath = sessionClient.sessionPath(projectId, sessionId + userID);

        // what's event
        const request = {
            session: sessionPath,
            queryInput: {
                event: {
                    name: event,
                    parameters: structjsnon.jsonToStructProto(parameters),
                    languageCode: languageCode
                }
            }
        };
        let responses = await sessionClient.detectIntent(request);
        responses = await self.handleAction(responses);
        return responses;
    },

    handleAction: function (responses) {
        let self = module.exports;
        let queryResult = responses[0].queryResult;
        // what's action ..'
        console.log('Action ')
        console.log(queryResult.action)
        switch (queryResult.action) {
            case 'Recommendation-yes':
                if (queryResult.allRequiredParamsPresent) {
                    self.saveRegistration(queryResult.parameters.fields);

                }
            case 'request':
                if (queryResult.allRequiredParamsPresent) {
                    self.saveRequest(queryResult.parameters.fields);

                }
                break;

        }

        return responses;
    },

    saveRegistration: async function (fields) {
        const registration = new Registration({
            lastname: fields.lastname.stringValue,
            phone: fields.phone.stringValue,
            email: fields.email.stringValue,
            dateSent: Date.now()
        });
        try {
            let reg = await registration.save((error) => {
                if (error) {
                    console.log('requests not saved to db ');
                } else {
                    console.log(fields);
                    console.log('success');
                }
            });
            console.log(reg);
        } catch (err) {
            console.log(err);
        }
    },

    saveRequest: async function (fields) {
        const request = new Request({
            request: fields.request.stringValue,
            roomno: fields.roomno.stringValue
        });
        try {
            let reg = await request.save((error) => {
                if (error) {
                    console.log('requests not saved to db ');
                } else {
                    console.log(fields);
                    console.log('success');
                }
            });
            console.log(reg);
        } catch (err) {
            console.log(err);
        }
    }
};