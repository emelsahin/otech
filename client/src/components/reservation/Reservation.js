import React from 'react';

const Reservation = () => (
    <div>
        <div style={{textAlign: 'center'}}>
            <h1>Enjoy your holiday with us!</h1>
        </div>
        <div className="row">
            <div className="col s6">
                <div className="card">
                    <div className="card-image">
                        <img
                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSLAyrYj6HDR5lQrL0g2C1gFkPh-xpHkyzjIBONXtdgb3b32n8psg&s" alt=""
                            width="240"
                            height="240"/>
                    </div>
                    <div className="card-content">
                        <div className="center-align">
                            <h4 className="red-text text-darken-2">Early Booking (25% discount December-February)
                            </h4>
                        </div>
                        <div className="center-align">
                            <p>for 90+ days early booking you get 25% OFF in your Surf Camp or surf course in
                                Fuerteventura
                                The discount is valid on all types of surf camps (in shared rooms, single and double
                                room) and on all types of surf courses.

                                You can book and prove the final price from our BOOKING FORM

                                For any information or doubt, CONTACT US</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col s6">
                <div className="card">
                    <div className="card-image">
                        <img
                            src="https://apps.rixos.com/Repo/List/4981-20200131-7f947abb-d490-46b5-899e-91c558171366.jpg" alt=""
                            width="240"
                            height="240"/>
                    </div>
                    <div className="card-content">
                        <div className="center-align">
                            <h4 className="red-text text-darken-2">Honeymoon Diamond Package(20% discount)</h4>
                        </div>
                        <div className="center-align">
                            <p>
                                Unforgettable memories at your honeymoon!
                                Unforgettable couple massage package :
                                Traditional Turkish Hammam (Traditional peeling & foam massage)
                                Bali Massage (50 min) & Jacuzzi
                                % 25 extra discount for Rixos Anjana SPA other usages.
                                Early check-in & Late check-out
                                Special Breakfast in room on preferred day and newspaper
                                Early check-in & late c-out
                            </p>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>


)


export default Reservation;