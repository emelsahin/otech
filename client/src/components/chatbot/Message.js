import React from 'react';

const Message = (props) => {
    return (
        <div className="cols s12 m8 offset-m2 offset-l3">
            <div className="card-panel grey lighten-5 z-depth-1">
                <div className="row valign-wrapper">
                    {props.speaks === 'bot' &&
                    <div className="col s2">
                        <img src="http://res.cloudinary.com/yemiwebby-com-ng/image/upload/v1529185121/bot_gphhi8.png"
                             alt="bot" width="55" height="55"/>
                    </div>
                    }
                    <div className="col s10">
                    <span className="black-text">
                        {props.text}
                    </span>
                    </div>
                    {props.speaks === 'user' &&
                    <div className="col s2">
                        <img
                            src="http://res.cloudinary.com/yemiwebby-com-ng/image/upload/v1529185121/user-new_fm7q4a.png"
                            alt="user" width="55" height="55"/></div>
                    }
                </div>
            </div>
        </div>
    );
};

export default Message;