import React from 'react';

const About = () => (
    <div className="row">

        <div className="col s12 m4 l8">
            <div className="center-align">

                <div className="card">
                    <div className="card-image">
                        <img src="https://static.wixstatic.com/media/f5f92a_a2fc471050584e3eba9de66f9e3ede94~mv2.png" alt=""
                             width="600"
                             height="600"/>
                    </div>
                    <div className="card-content">
                        <p>The Rixos Group has experienced substantial growth over recent years as a direct result of
                            dynamic, flexible and proactive strategic management. Provide uniquely authentic services
                            and
                            experiences; engaging guests of all ages to create memorable journeys in our hotels &
                            resorts.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

)


export default About;