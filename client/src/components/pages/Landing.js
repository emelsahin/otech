import React from 'react';

const Landing = () => (
    <div>
        <div style={{textAlign: 'center'}}>
            <h1>Enjoy your holiday with us!</h1>
        </div>
        <div className="row">
            <div className="col s12 m4">
                <div className="card">
                    <div className="card-image">
                        <img src="https://www.ahstatic.com/photos/b0k0_rotwa_00_p_1024x768.jpg" alt="" width="240"
                             height="240"/>
                        <span className="card-title">Single Room</span>
                    </div>
                    <div className="card-content">
                        <p>1 bedroom (37 sqm) <br/>
                            1 bathroom <br/>
                            Minibar
                            Balcony
                            Central air conditioner, safe box
                            Marbled and carpeted floor
                            Tea & Coffee set-up
                            Bathroom
                            Shower bath, WC
                            Make up mirror and hair dryer
                            Technology
                            Mirror TV, satellite broadcast, music broadcast
                            Internet connection and telephone</p>
                        <div className="center-align">
                            <h4 className="red-text text-darken-2">$100/night</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col s12 m4">
                <div className="card">
                    <div className="card-image">
                        <img
                            src="https://q-xx.bstatic.com/xdata/images/hotel/max500/160486781.jpg?k=6a6c69c6bf8fba43aff819648010e8d5945c09d1475ccc9190fd749359a0fa72&o="
                            alt=""
                            width="240"
                            height="240"/>
                        <span className="card-title">Double Room</span>
                    </div>
                    <div className="card-content">
                        <p>Total area of 49 sqm <br/>
                            2 bedrooms <br/>
                            1 bathroom <br/>
                            Minibar,
                            Tea & coffee set-up,
                            Balcony,
                            Central air conditioner, safe box,
                            Marble and carpeted floor,
                            Bathroom,
                            Shower bath, WC,
                            Make up mirror and hair dryer,
                            Technology,
                            Mirror TV,music broadcast,
                            Internet connection and telephone
                        </p>
                        <div className="center-align">
                            <h4 className="red-text text-darken-2">$200/night</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col s12 m4">
                <div className="card">
                    <div className="card-image">
                        <img src="https://images.elitema.com.tr/db_images/194/12/220/accommodation18_1600x1066.jpg"
                             alt=""
                             width="240"
                             height="240"/>
                        <span className="card-title">King Room</span>
                    </div>
                    <div className="card-content">
                        <p>Total area of 195 <br/>
                            1 bedroom <br/>
                            1 sitting room, 1 bathroom <br/>
                            Minibar,
                            Tea & coffee maker,
                            Terrace,
                            Central air conditioner, safe box,
                            Marble carpeted floor,
                            Bathroom,
                            Shower bath, 2 WC units,
                            Make up mirror, hair dryer,
                            Technology,
                            2 TV, music broadcast
                            Internet connection, telephone
                        </p>
                        <div className="center-align">
                            <h4 className="red-text text-darken-2">$500/night</h4>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
)

export default Landing;