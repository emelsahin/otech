const mongoose = require('mongoose');
const {Schema} = mongoose;

const requestSchema = new Schema({
    request: String,
    roomno: Number
});

module.exports =   mongoose.model('Request', requestSchema);

