const mongoose = require('mongoose');
const { Schema } = mongoose;

const registrationSchema = new Schema({
    lastname: String,
    phone: String,
    email: String,
    registerDate: Date
});

module.exports= mongoose.model('Registration', registrationSchema);